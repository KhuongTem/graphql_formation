FROM node:9.8-alpine

WORKDIR /usr/src/graph

EXPOSE 3000
CMD ["yarn", "run", "serve"]

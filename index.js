import express from 'express'
import bodyParser from 'body-parser'
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express'
import schema from './presentation/schema'
import Hero from './bussiness/hero'

const PORT = 3000

const app = express()

app.use('/graphql', bodyParser.json(),graphqlExpress({
  schema,
  context: {
    dataLoaders: {
      hero: Hero.getLoaders(),
    }
  },
  debug: true
}))
app.get('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))

app.listen(PORT)
